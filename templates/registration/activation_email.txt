{% load i18n %}

Bonjour,

Nous avons reçu une demande de création de compte citoyen. 
Pour confirmer la création de votre compte citoyen, visitez simplement cette page :
{{ registration_url }}

Ce lien est valide pendant {{ expiration_days }} jours. Au-delà, la demande de création de compte sera annulée.

Si vous ne souhaitez pas créer de compte citoyen, ignorez simplement ce message. Si vous pensez que vous avez été inscrit à votre insu, ou pour toute autre question, envoyez-nous un courriel à comptecitoyen@vincennes.fr

Ce message vous a été envoyé automatiquement, merci de ne pas utiliser la fonction « répondre ».

-- 
Service administration électronique
Mairie de Vincennes
53 bis rue de Fontenay- 94300 Vincennes
Accueil : 01 43 98 65 00
comptecitoyen@vincennes.fr
www.vincennes.fr


