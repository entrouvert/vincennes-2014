Ce message vous a été envoyé automatiquement, merci de ne pas utiliser la fonction « répondre ».


Vous avez demandé à changer votre adresse de courriel, de :

  {{ user.email }}

à :

  {{ email }}

Pour valider cette modification, veuillez cliquer sur le lien suivant :

  {{ link }}

-- 
Service administration électronique
Mairie de Vincennes
53 bis rue de Fontenay- 94304 Vincennes Cedex
01 43 98 69 53
comptecitoyen@vincennes.fr
www.vincennes.fr
