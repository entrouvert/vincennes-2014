#! /usr/bin/env python

import json
import requests

from hobo.deploy.signals import notify_agents
from hobo.environment.models import Variable

PARTS = ['menu', 'demarches', 'partenaires', 'agenda', 'magazine', 'autopromo',
        'services', 'acces-directs', 'kiosque', 'grand-projets', 'actualites']

for part in PARTS:
    r = requests.get('https://www.vincennes.fr/api/data/(Element)/' + part)
    r.raise_for_status()
    variable, created = Variable.objects.get_or_create(
            name=part.replace('-', '_'),
            defaults={'auto': True})
    if variable.json == r.json():
        continue
    variable.value = json.dumps(r.json())
    variable.save()

notify_agents(None)
