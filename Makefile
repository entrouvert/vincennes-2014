VERSION=`git describe | sed 's/^v//; s/-/./g' `
NAME="vincennes-themes"

prefix = /usr

all:
	@(echo "Nothing to build. Please use make install.")

clean:
	rm -rf build sdist

build: clean
	mkdir -p build/$(NAME)-$(VERSION)
	for i in *; do \
		if [ "$$i" != "build" ]; then \
			cp -R "$$i" build/$(NAME)-$(VERSION); \
		fi; \
	done

install:
	mkdir -p $(DESTDIR)$(prefix)/share/authentic2/vincennes
	mkdir -p $(DESTDIR)$(prefix)/share/publik/themes/vincennes-2014
	mkdir -p $(DESTDIR)$(prefix)/share/wcs/themes/vincennes-2014
	mkdir -p $(DESTDIR)$(prefix)/sbin/
	cp update-vincennes-content.py $(DESTDIR)$(prefix)/sbin/
	cp themes.json $(DESTDIR)$(prefix)/share/publik/themes/vincennes-2014
	cp -r idp/* $(DESTDIR)$(prefix)/share/authentic2/vincennes
	cp -r static templates $(DESTDIR)$(prefix)/share/publik/themes/vincennes-2014
	cp -r desc.xml wcs.css dataview.js $(DESTDIR)$(prefix)/share/wcs/themes/vincennes-2014

dist-bzip2: build
	mkdir sdist
	cd build && tar cfj ../sdist/$(NAME)-$(VERSION).tar.bz2 .

version:
	@(echo $(VERSION))

name:
	@(echo $(NAME))

fullname:
	@(echo $(NAME)-$(VERSION))

